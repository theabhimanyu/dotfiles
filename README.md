# dotfiles

[![docs](https://img.shields.io/badge/🗐-website-blue?style=for-the-badge)](https://ExpandingMan.gitlab.io/dotfiles/)

Linux configuration.  See the website for details and a curated list of software.

The installer can be run with `./install.sh`.  Run `./install.sh julia` to install Julia
to `/opt/julia`.
