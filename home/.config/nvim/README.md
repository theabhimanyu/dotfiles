# Neovim Configuration
The neovim config became sufficiently elaborate to warrant its own repository, which can be found at [neovimconfig](https://gitlab.com/ExpandingMan/neovimconfig).

The installer configured via this repository will clone that repo.
