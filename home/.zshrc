# configuration now kept in .bashrc so it works in both shells
source "$HOME/.bashrc"

rprompt() {
    o1=`starship module git_status`
    o2=`starship module git_branch`
    o3=`starship module git_commit`
    if [[ -n $o2 ]]; then
        echo "$o1$o2$o3"
    fi
}

if [[ $0 =~ zsh ]]; then
    if [[ -x `which starship` ]]; then
        eval "$(starship init zsh)"
        #TODO: not sure how to get it to evaluate here
        export RPROMPT=""
    fi
fi
